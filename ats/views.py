from django.views.generic import ListView
from rest_framework import generics

from ats.documents import JobApplicationDocument
from ats.models import JobApplication
from ats.serializer import JobApplicationSerializer


class JobApplicationList(ListView):
    context_object_name = 'job_list'
    template_name = "base.html"

    def get_queryset(self):
        docs = JobApplicationDocument.search()
        return docs.to_queryset()


class JobApplicationUpdateView(generics.UpdateAPIView):
    #TODO: OPTIMIZE QUERIES
    queryset = JobApplication.objects.all()
    serializer_class = JobApplicationSerializer


class JobApplicationFilterView(generics.GenericAPIView):
    pass


