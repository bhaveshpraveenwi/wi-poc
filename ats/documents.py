# documents.py

from django_elasticsearch_dsl import DocType, Index
from ats.models import JobApplication


# Name of the Elasticsearch index
car = Index('job_applications')
# See Elasticsearch Indices API reference for available settings
car.settings(
    number_of_shards=1,
    number_of_replicas=0
)

@car.doc_type
class JobApplicationDocument(DocType):
    class Meta:
        model = JobApplication  # The model associated with this DocType

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'status',
        ]