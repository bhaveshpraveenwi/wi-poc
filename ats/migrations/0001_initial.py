# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Candidate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='Full Name', max_length=100)),
                ('location', models.CharField(verbose_name='Location', max_length=100)),
                ('experience', models.PositiveIntegerField(verbose_name='Experience', choices=[(1, 'No Experience'), (2, '0-6 months Experience'), (3, '1-2 years experience'), (4, 'MORE_THAN_FOUR')])),
            ],
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('title', models.CharField(verbose_name='Title', max_length=256)),
                ('description', models.CharField(verbose_name='Description', max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='JobApplication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('candidate', models.OneToOneField(to='ats.Candidate')),
                ('job', models.ForeignKey(to='ats.Job')),
            ],
        ),
    ]
