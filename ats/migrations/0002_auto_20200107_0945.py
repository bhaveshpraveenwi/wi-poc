# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ats', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobapplication',
            name='status',
            field=models.PositiveIntegerField(verbose_name='Job Application Status', default=1, choices=[(1, 'No Status'), (2, 'Shortlisted'), (3, 'Hired'), (4, 'Rejected')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='candidate',
            name='experience',
            field=models.PositiveIntegerField(verbose_name='Experience', choices=[(1, 'No Experience'), (2, '0-6 months Experience'), (3, '1-2 years experience'), (4, 'More than 4')]),
        ),
    ]
