from django.db import models
from django.utils.translation import ugettext_lazy as _


class Job(models.Model):
    title = models.CharField(max_length=256, verbose_name=_('Title'))
    description = models.CharField(max_length=256, verbose_name=_('Description'))

    def __str__(self):
        return f'{self.id}'

    class Meta:
        ordering = ('-id', )


class Candidate(models.Model):
    NO_EXPERIENCE = 1
    UPTO_SIX_MONTHS_EXPERIENCE = 2
    TWO_TO_FOUR_YEARS_EXPERIENCE = 3
    MORE_THAN_FOUR = 4

    EXPERIENCE_CHOICES = (
        (NO_EXPERIENCE, _('No Experience')),
        (UPTO_SIX_MONTHS_EXPERIENCE, _('0-6 months Experience')),
        (TWO_TO_FOUR_YEARS_EXPERIENCE, _('1-2 years experience')),
        (MORE_THAN_FOUR, _('More than 4'))
    )

    name = models.CharField(max_length=100, verbose_name=_('Full Name'))
    location = models.CharField(max_length=100, verbose_name=_('Location'))
    experience = models.PositiveIntegerField(choices=EXPERIENCE_CHOICES, verbose_name='Experience')

    def __str__(self):
        return f'{self.id}'

    class Meta:
        ordering = ('-id', )


class JobApplication(models.Model):
    NO_STATUS = 1
    SHORTLISTED = 2
    HIRED = 3
    REJECTED = 4

    STATUS_CHOICES = (
        (NO_STATUS, _('No Status')),
        (SHORTLISTED, _('Shortlisted')),
        (HIRED, _('Hired')),
        (REJECTED, _('Rejected')),
    )

    candidate = models.OneToOneField('Candidate', on_delete=models.CASCADE)
    job = models.ForeignKey('Job', on_delete=models.CASCADE)
    status = models.PositiveIntegerField(choices=STATUS_CHOICES, verbose_name=_('Job Application Status'))

    def __str__(self):
        return f'{self.id}'

    class Meta:
        ordering = ('-id', )

