from collections import OrderedDict

from rest_framework import fields, serializers


class ChoicesField(serializers.Field):
    """Custom ChoiceField serializer field."""

    def __init__(self, choices, **kwargs):
        """init."""
        self._choices = OrderedDict(choices)
        # print('choices', choices)
        super(ChoicesField, self).__init__(**kwargs)

    def to_representation(self, obj):
        """Used while retrieving value for the field."""
        return self._choices[obj]

    def to_internal_value(self, data):
        """Used while storing value for the field."""
        for choice in self._choices:
            print(choice, data)
            if str(choice) == data or self._choices[choice].lower() == data.lower():
                return choice
        raise serializers.ValidationError("Acceptable values are {}.".format(list(str(choice) for choice in self._choices.values())))