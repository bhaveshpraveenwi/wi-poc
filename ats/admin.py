from django.contrib import admin

from ats import models

admin.site.register(models.Job)
admin.site.register(models.Candidate)
admin.site.register(models.JobApplication)