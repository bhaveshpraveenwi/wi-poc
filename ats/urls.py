from django.conf.urls import include, url
from django.contrib import admin

from ats import views as ats_views

urlpatterns = [
    # Examples:
    # url(r'^$', 'ats.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^jobs/filter', ats_views.JobApplicationFilterView.as_view()),
    url(r'^jobs/(?P<pk>\d+)/', ats_views.JobApplicationUpdateView.as_view()),
    url(r'^jobs/$', ats_views.JobApplicationList.as_view()),

    # url(r'^filter/jobs/', ats_views.JobApplicationRetrieveView.as_view()),

    url(r'^admin/', include(admin.site.urls)),
]
