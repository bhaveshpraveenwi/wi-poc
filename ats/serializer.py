from rest_framework import serializers, fields

from ats.fields import ChoicesField
from ats.models import JobApplication


class JobApplicationSerializer(serializers.ModelSerializer):
    status = ChoicesField(choices=JobApplication.STATUS_CHOICES)

    class Meta:
        model = JobApplication
        fields = ('job', 'candidate', 'status')
